import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingFillInTheBlankQuestion {
    @Test
    public void testCapitalization(){
        FillInTheBlank fib = new FillInTheBlank("question", "answer", "answer");
        double f = fib.getPoints();
        assertEquals(1, f);
        FillInTheBlank fib2 = new FillInTheBlank("question", "ANSWER", "answer");
        double f2 = fib2.getPoints();
        assertEquals(1, f2);
        FillInTheBlank fib3 = new FillInTheBlank("question", "answer", "ANSWER");
        double f3 = fib3.getPoints();
        assertEquals(1, f3);
    }
    @Test
    public void testIncorrect(){
        FillInTheBlank fib = new FillInTheBlank("question", "answer", "answerr");
        double f = fib.getPoints();
        assertEquals(0, f);
        FillInTheBlank fib2 = new FillInTheBlank("question", "ANSWER", "answers");
        double f2 = fib2.getPoints();
        assertEquals(0, f2);
    }
    
}
