/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
public abstract class QuestionWithChoice implements Question {
    protected String question;
    protected String answer;
    protected String userAnswer;
    protected double maxPoints;
    protected double gottenPoints;

    @Override
    public void readQuestion(){
        System.out.println(this.question);
    };

    @Override
    public double getPoints(){
        if(this.userAnswer.toLowerCase().equals(this.answer.toLowerCase())){
            return maxPoints;
        }
        else{
            return 0;
        }

    }
    @Override
    public String toString(){
        return 
                  "question is: " + this.question 
                + " correct answer is: " + this.answer 
                + " given answer is: " + this.userAnswer 
                + " the max points for this question is: " + this.maxPoints 
                + " the points obtained are: " + this.gottenPoints;
    }
    public String getAnswer(){
        return this.answer;
    }
    public String getUserAnswer(){
        return this.userAnswer;
    }
    public double getMaxPoints(){
        return this.maxPoints;
    }
    public double getGottenPoints(){
        return this.gottenPoints;
    }
}
