import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingMultipleChoiceQuestions {
    @Test
    public void TestConstructorWithTwoInvalidInputs(){
        try{
            MultipleChoiceQuestion mult = new MultipleChoiceQuestion("question", "ab", "ba");
            fail("should have caught an exception but didn't");
        }
            catch(IllegalArgumentException e){
                //did what it was supposed to
            }
            catch(Exception e){
                fail("threw exception of the wrong type.");
            }
        }
        @Test
        public void TestConstructorWithFirstInvalidInputs(){
            try{
                MultipleChoiceQuestion mult = new MultipleChoiceQuestion("question", "ab", "a");
                fail("should have caught an exception but didn't");
            }
                catch(IllegalArgumentException e){
                    //did what it was supposed to
                }
                catch(Exception e){
                    fail("threw exception of the wrong type.");
                }
            }
            @Test
            public void TestConstructorWithSecondInvalidInputs(){
                try{
                    MultipleChoiceQuestion mult = new MultipleChoiceQuestion("question", "a", "ba");
                    fail("should have caught an exception but didn't");
                }
                    catch(IllegalArgumentException e){
                        //did what it was supposed to
                    }
                    catch(Exception e){
                        fail("threw exception of the wrong type.");
                    }
                }
        @Test
        public void testGetPointsCorrect(){
            //should be case insensitive when correct:
            MultipleChoiceQuestion mult = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "a", "a");
            double t = mult.getPoints();
            assertEquals(1, t);
            MultipleChoiceQuestion mult2 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "A", "a");
            double f = mult2.getPoints();
            assertEquals(1, f);
            MultipleChoiceQuestion mult3 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "a", "A");
            double k = mult3.getPoints();
            assertEquals(1, k);
            MultipleChoiceQuestion mult4 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "A", "A");
            double l = mult4.getPoints();
            assertEquals(1, l);
           
        }
        @Test
        public void testGetPointsIncorrect(){
             //should be case insensitive when incorrect:
             MultipleChoiceQuestion fault = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "a", "b");
             double f1 = fault.getPoints();
             assertEquals(0, f1);
             MultipleChoiceQuestion fault2 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "A", "b");
             double f2 = fault2.getPoints();
             assertEquals(0, f2);
             MultipleChoiceQuestion fault3 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "a", "B");
             double f3 = fault3.getPoints();
             assertEquals(0, f3);
             MultipleChoiceQuestion fault4 = new MultipleChoiceQuestion("the letter after A is: a:B) b:C) c:D) d:G)", "A", "B");
             double f4 = fault4.getPoints();
             assertEquals(0, f4);
        }

        
    }
    

