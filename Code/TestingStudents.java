import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingStudents {
    @Test
    public void testAddAStudent(){
        Students classroom = new Students();
        classroom.addAStudent("John Glee");

    }
    @Test
    public void testValidateIfStudentExists1(){
        Students classroom = new Students();
        classroom.addAStudent("Funny Valentine");
        classroom.addAStudent("John Glee");
        classroom.addAStudent("Jonathan Joestar");
        boolean checker = classroom.validateIfStudentExists("John Glee");
        assertEquals(true, checker);
    }
    @Test
    public void testValidateIfStudentExists2(){
        Students classroom = new Students();
        classroom.addAStudent("John Glee");
        classroom.addAStudent("Funny Valentine");
        classroom.addAStudent("Jonathan Joestar");
        boolean checker = classroom.validateIfStudentExists("Mike Michael");
        assertEquals(false, checker);
    }
    @Test
    public void testSearchForStudent(){
        Student testStudent = new Student("Funny Valentine");
        Students classroom = new Students();
        classroom.addAStudent("John Glee");
        classroom.addAStudent("Funny Valentine");
        classroom.addAStudent("Jonathan Joestar");
        Student actualStudent = classroom.searchForStudent("Funny Valentine");
        assertEquals(testStudent.getName(), actualStudent.getName());
    }
    @Test
    public void testFindStudentIndex(){
        Students classroom = new Students();
        classroom.addAStudent("John Glee");
        classroom.addAStudent("Funny Valentine");
        classroom.addAStudent("Jonathan Joestar");
        int checker = classroom.findStudentIndex("Jonathan Joestar");
        assertEquals(2, checker);
    }
}
