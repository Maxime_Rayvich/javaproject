/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.ArrayList;
public class ExamManager {
    private String examTaker;
    private int examNumber;
    private String answersURL;
    private String examURL; //note: path from folder 'javaproject'.
    private List<String> answers = new ArrayList<String>();;
    //private boolean ranPrompt; //this gets set to true only if promptUserToAnswer is run.
    
    //constructor
    public ExamManager(String examTaker, int examNumber, String url){
        Path p = Paths.get(url);
        if(!Files.exists(p)){
            throw(new IllegalArgumentException("Invalid path for exam. " + p.toAbsolutePath() + " given."));
        }
        if(examNumber < 1){
            throw(new IllegalArgumentException("Can not have negative number as the number of the exam"));
        }
        this.examTaker = examTaker;
        this.examNumber = examNumber;
        this.answersURL = makeURL(examTaker, examNumber);
        this.examURL = url;
        //this.ranPrompt = false;
    }
    /**
     * Takes as input String and int and returns a String in the format of UsersAnswers\\person_number.txt
     * @param {String}
     * @param {int}
     * @return String
     */
    public String makeURL(String person, int number){
       final String PATH = "UsersAnswers\\";
        String url = PATH + person + "_" + number + ".txt";
        return url;
    }

    /**
     * It's a to string
     */
    @Override
    public String toString(){
        return "examTaker is: " + this.examTaker + " examNumber is: " + this.examNumber + " answers URL is: " + this.answersURL + " exam URL is: " + this.examURL;
    }
    /**
     * given a line number, returns the question that is located at that line number.
     * @param lineNumber {int}
     * @param position {int}
     * @return {String}
     * @throws IOException
     */
    public String getLineAtSpecificPosition(int lineNumber, int position) throws IOException{
        final int NUMBER_OF_SPOTS_FOR_PIECES = 3;
        String question = "";
        List<String> questions = readQuestionsFromFile();
        if (lineNumber >= questions.size()){
            throw new IllegalArgumentException("The file stops at " + questions.size() + " lines. Please enter a number in its range.");
        }
        String[] pieces = new String[NUMBER_OF_SPOTS_FOR_PIECES];
        String line = questions.get(lineNumber);
        pieces = line.split(";");
        if(position > NUMBER_OF_SPOTS_FOR_PIECES){
            throw(new IllegalArgumentException("The last position for each question is " + NUMBER_OF_SPOTS_FOR_PIECES + ". Please enter a number in its range."));
        }
        question = pieces[position];
        return question;

    }  
    public String getCorrectAnswerFromFile(int lineNumber) throws IOException{
        final int POSITION_OF_ANSWER = 2;
        String answer = getLineAtSpecificPosition(lineNumber, POSITION_OF_ANSWER);
        return answer;
    }


    /**
     * Takes as input a line number and returns the question that is located at that point (because that is how our exams are structured: type of question;question...)
     * @param lineNumber {int}
     * @return {String}
     * @throws IOException
     */
    public String getQuestionLineFromFile(int lineNumber) throws IOException{
        final int POSITION_OF_QUESTION = 1;
        String question = getLineAtSpecificPosition(lineNumber, POSITION_OF_QUESTION);
        return question;
    }
    /**
     * takes a String and adds it to the answers field.
     * @param answer {String}
     */
    public void addToAnswers(String answer){
        this.answers.add(answer);
    }

    /**
     *Takes as input a line number and returns the question type that is located at that point (because that is how our exams are structured: type of question;question...)
     * @param lineNumber {int}
     * @return {String}
     * @throws IOException
     */
    public String getQuestionType(int lineNumber) throws IOException{
        final int POSITION_OF_QUESTION_TYPE = 0;
        String questionType = getLineAtSpecificPosition(lineNumber, POSITION_OF_QUESTION_TYPE);
        return questionType;
    }
    

    //returns a list<String> using the exam URL.

    /**
     * returns a list<String> by extracting it from the exam url provided.
     * @return {List<String>}
     * @throws IOException
     */
    public List<String> readQuestionsFromFile() throws IOException{
        List<String> questions = null;
        try{
            questions = extract_file(this.examURL);
        }
        catch(IOException e){
            System.out.println("Caught IO exception " + e + " in readQuestionsFromFile");
        }
        return questions;
    }
    /**
     * Generates a file with the user's answers, stores it in UsersAnswers folder.
     * Takes as input an int representing the percentage of the student's grade.
     * @param grade {int}
     * @throws IOException
     */
    public void createAnswerFile(double grade) throws IOException{
        List<String> questionsAndAnswers = new ArrayList<String>();
        for(int i = 0; i < this.getAnswers().size(); i++){
            questionsAndAnswers.add(getQuestionLineFromFile(i) + " user answered: " + getAnswers().get(i) + " correct answer is: " + getCorrectAnswerFromFile(i));
        }
        questionsAndAnswers.add("Your grade is: "+ grade);
        String url = this.getAnswersURL();
        try{
                Path p = Paths.get(url);

                System.out.println(p.toAbsolutePath());

            Files.write(p, questionsAndAnswers);                
        }
        catch(IOException e){
            System.out.println("Caught IO Exception " + e + " in createAnswerFile");
        }
    }
    
    
    /**
     * Takes as input a String with the path to a file, returns list<String> of things in that file.
     * @param path {String}
     * @return {List<String>}
     * @throws IOException
     */
    public List<String> extract_file(String path) throws IOException{
        List<String> file = null;
        Path p = Paths.get(path);
       // System.out.println(p.toAbsolutePath());

    try{

        file = Files.readAllLines(p);
    }
    catch(IOException e){
        System.out.println("Caught IO Exception " + e + " in extract_file");
        
    }
    return file;
    }

    /**
     * Takes as input a List<String> path and prints out its contents (we did not use it for the application.)
     * @param path {List<String>}
     */
    public void readList(List<String> path){
    for(int i = 0; i < path.size(); i++){
        System.out.println(path.get(i));
    }
}

    /**
     * this method combines the questions from the url with the answers given to create a Question[]
     * @return Question[]
     * @throws IOException
     */
    public Question[] getQuestionsArr() throws IOException{
            //becuase this uses the answers field which is not initialy initialized.
            if(this.answers == null){
                throw(new NullPointerException("This object's size property has not yet been initialized."));
            }
            else{
            Question[] quests = new Question[this.answers.size()];
            final String TRUE_OR_FALSE = "t";
            final String MULTIPLE_CHOICE = "m";
            final String FILL_IN_THE_BLANK = "f";
          //getting a List<String> of the lines in the exam doc.  
          List<String> questions = extract_file(this.examURL);
            
            String line = "";
            String[] pieces = new String[4];
            for(int i = 0; i < quests.length; i++){
                line = questions.get(i);
                pieces = line.split(";");
                if(pieces[0].equals(TRUE_OR_FALSE)){
                    quests[i] = new TrueOrFalseQuestion(pieces[1], pieces[2], this.answers.get(i));
                }
                else if(pieces[0].equals(MULTIPLE_CHOICE)){
                    quests[i] = new MultipleChoiceQuestion(pieces[1], pieces[2], this.answers.get(i));
                }
                else if(pieces[0].equals(FILL_IN_THE_BLANK)){
                    quests[i] = new FillInTheBlank(pieces[1], pieces[2], this.answers.get(i)); 
                }
                else{
                    throw(new IllegalArgumentException("The category for questions must be 'm' (multiple choice) 'f' (fill in the blank) or 't' (true/false)"));
                }
            }
            return quests;
        }
    }      
    /**
     * getters 
    */
    public List<String> getAnswers(){
        return this.answers;
    }
    public String getAnswersURL(){
        return this.answersURL;
    }
    public String getExamURL(){
        return this.examURL;
    }
    public int getExamNumber(){
        return this.examNumber;
    }

}

