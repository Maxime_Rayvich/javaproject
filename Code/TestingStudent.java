import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingStudent {
    @Test
    public void testReturnName(){
        Student student = new Student("John Glee");
        assertEquals("John Glee", student.getName());
    }
    @Test
    public void testGetGrade(){
        Student student = new Student("John Glee");
        double[] doubleArr = new double[]{0,0,0};
        double[] doubleStudentArr = student.getGrade();
        boolean checker = true; 
        for(int i = 0; i < doubleArr.length; i++){
            if( ! (doubleArr[i] == doubleStudentArr[i])){
                checker = false;
            }
        }
        assertEquals(true, checker);
    }
    @Test
    public void testAddGrade(){
        Student student = new Student("John Glee");
        student.addGrade(60, 1);
        student.addGrade(70, 2);
        student.addGrade(80, 3);
        double[] doubleArr = new double[]{60,70,80};
        double[] doubleStudentArr = student.getGrade();
        boolean checker = true; 
        for(int i = 0; i < doubleArr.length; i++){
            if( ! (doubleArr[i] == doubleStudentArr[i])){
                checker = false;
            }
        }
        assertEquals(true, checker);
    }
    @Test
    public void testGetAverage(){
        Student student = new Student("John Glee");
        student.addGrade(60, 1);
        student.addGrade(70, 2);
        student.addGrade(80, 3);
        double average = student.getAverage();
        assertEquals(70, average);
    }
}
