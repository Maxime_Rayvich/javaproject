/**
 * @author Maxime Rayvich, Bastian Fernandez Cortez
 * @version 2021-11-30
 */
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.control.TextArea;

public class HboxGenerator {
    private HBox hbox;

    public HboxGenerator(String questionType, String question) {
        this.hbox = generateHbox(questionType, question);
    }

    public HBox generateHbox(String questionType, String question) {
        if (!questionType.equals("m") && !questionType.equals("f") && !questionType.equals("t")) {
            throw (new IllegalArgumentException("Question type must be 'f', 'm', or 't'."));
        }

        final String MULTIPLE_CHOICE = "m";
        final String FILL_IN_THE_BLANK = "f";
        HBox box = null;
        if (questionType.equals(MULTIPLE_CHOICE)) {
            box = getMultipleChoiceBox(question);
        } else if (questionType.equals(FILL_IN_THE_BLANK)) {
            box = getFillInTheBlankBox(question);
        } else {
            box = getTrueOrFalseBox(question);
        }
        return box;
    }

    /**
     * Generates an hbox with the question on one side and the answer format on the other.
     * In this case, it's a multiple choice question, so it will be 4 buttons A,B,C,D
     * @param question {String}
     * @return
     */
    public HBox getMultipleChoiceBox(String question) {
        ToggleGroup group = new ToggleGroup();
        HBox buttonHolder = new HBox();
        RadioButton buttonA = new RadioButton("A");
        buttonA.setToggleGroup(group);
        buttonA.setStyle("-fx-text-fill: white;-fx-padding: 10");
        RadioButton buttonB = new RadioButton("B");
        buttonB.setToggleGroup(group);
        buttonB.setStyle("-fx-text-fill: white;-fx-padding: 10");
        RadioButton buttonC = new RadioButton("C");
        buttonC.setToggleGroup(group);
        buttonC.setStyle("-fx-text-fill: white;-fx-padding: 10");
        RadioButton buttonD = new RadioButton("D");
        buttonD.setToggleGroup(group);
        buttonD.setStyle("-fx-text-fill: white;-fx-padding: 10");
        buttonHolder.getChildren().addAll(buttonA, buttonB, buttonC, buttonD);
        TextArea textArea = new TextArea(question);
        textArea.setPrefHeight(40);
        HBox everythingHolder = new HBox();
        everythingHolder.setPrefWidth(567);
        everythingHolder.getChildren().addAll(textArea, buttonHolder);
        return everythingHolder;
    }
     /**
     * Generates an hbox with the question on one side and the answer format on the other.
     * In this case, it's a fill in the blank question, so it will be a text area.
     * @param question {String}
     * @return
     */
    public HBox getFillInTheBlankBox(String question) {
        TextField answerField = new TextField();
        answerField.setStyle("-fx-background-color: grey");
        answerField.setPrefWidth(257);
        TextArea questionField = new TextArea(question);
        questionField.setPrefHeight(40);
        HBox everythingHolder = new HBox();
        everythingHolder.setSpacing(10);
        everythingHolder.setPrefWidth(567);
        everythingHolder.getChildren().addAll(questionField, answerField);
        return everythingHolder;
    }
 /**
     * Generates an hbox with the question on one side and the answer format on the other.
     * In this case, it's a true or false choice question, so it will be 2 buttons true or false.
     * @param question {String}
     * @return
     */
    public HBox getTrueOrFalseBox(String question) {
        ToggleGroup group = new ToggleGroup();
        HBox buttonHolder = new HBox();
        RadioButton buttonTrue = new RadioButton("True");
        buttonTrue.setToggleGroup(group);
        buttonTrue.setStyle("-fx-text-fill: white;-fx-padding: 10");
        RadioButton buttonFalse = new RadioButton("False");
        buttonFalse.setToggleGroup(group);
        buttonFalse.setStyle("-fx-text-fill: white;-fx-padding: 10");
        buttonHolder.getChildren().addAll(buttonTrue, buttonFalse);
        TextArea textArea = new TextArea(question);
        textArea.setPrefHeight(40);
        HBox everythingHolder = new HBox();
        everythingHolder.setPrefWidth(567);
        everythingHolder.getChildren().addAll(textArea, buttonHolder);
        return everythingHolder;
    }
    public HBox getHBox(){
        return this.hbox;
    }

}
