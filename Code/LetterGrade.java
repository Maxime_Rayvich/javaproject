public class LetterGrade extends Grade {
    public String toLetter(){
        //Array with boundries and another with the String
        double[] gradeArray = {100, 90, 84, 80, 77, 73, 70, 67, 63, 60, 50};
        String[] gradeStringArray = {"A+","A","A-","B+","B","B-","C+","C","C-","D"};

        for(int i=0; i < gradeStringArray.length; i++){
            if(this.percentage <= gradeArray[i] && this.percentage >= gradeArray[i+1]){
                return gradeStringArray[i];
            }
        }
            return "F";
    }
    @Override
    public String toString(){
        return toLetter();
    }
}
