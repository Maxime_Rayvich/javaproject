import java.io.IOException;
import java.nio.file.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
/**
 * Creates a random exam consisting of: 
 * 3 fill in the blank
 * 3 multiple choice
 * 4 true or false
 * @author Kelsey Pereira Cosra
 */
public class RandomExamMaker {
    private final Path FILL_IN_THE_BLANK = Paths.get("Exams\\FillInTheBlank.txt");
    private final Path MULTIPLE_CHOICE = Paths.get("Exams\\MultipleChoice.txt");
    private final Path TRUE_OR_FALSE = Paths.get("Exams\\TrueOrFalse.txt");
    Random randy = new Random();

    /**
     * generates a random exam and returns 
     * @param examNumber int representing exam number 
     * @return a string representing the path of exam
     * @throws IOException
     */
    public String generateRandomExam(int examNumber) throws IOException{
        List<String> linesOfNewExam = constructExam();
        Files.write(Paths.get("Exams\\exam" + examNumber + ".txt"), linesOfNewExam);
        return "Exams\\exam" + examNumber + ".txt";
    }
    /**
     * reads the lines in a given path
     * @param p path to a source file
     * @return array list of strings of the read file lines
     */
    private List<String> readLines(Path p){
        List<String> lines = new ArrayList<String>();
        try {
            lines = Files.readAllLines(p);
            return lines;
        } catch (IOException e){
            System.out.println("UH OH! There was an error at readLines()" + e);
        }
        return lines;
    }
    /**
     * makes a random exam using the 3,3,4 exam conventions
     * @return array list of the final random exam
     */
    private List<String> constructExam(){
        List<String> constructExam = new ArrayList<String>();
        List<String> fillInBlank = readLines(this.FILL_IN_THE_BLANK);
        List<String> multipleChoice = readLines(this.MULTIPLE_CHOICE);
        List<String> trueFalse = readLines(this.TRUE_OR_FALSE);
        for(int i = 0; i < 3; i++){
            int randomNum = randy.nextInt(fillInBlank.size());
            constructExam.add(fillInBlank.get(randomNum));
        }
        for(int i = 0; i < 3; i++){
            int randomNum = randy.nextInt(multipleChoice.size());
            constructExam.add(multipleChoice.get(randomNum));
        }
        for(int i = 0; i < 4; i++){
            int randomNum = randy.nextInt(trueFalse.size());
            constructExam.add(trueFalse.get(randomNum));
        }
        return constructExam;
    }
}
