//Bastian Fernandez Cortez
import java.io.IOException;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.control.TextArea;
import javafx.event.ActionEvent;

public class ExamManagerSubmit implements EventHandler<ActionEvent>{
    private HBox questionAnswer;
    private HBox studentSpecifics;
    private HboxGenerator boxGen;
    private ExamManager manager;
    private Students studentList;
    private int counter;
    private boolean check;
    private String studentName;
    public ExamManagerSubmit(HBox questionAnswer, HBox studentSpecifics, String studentName, Students studentList){
        this.questionAnswer = questionAnswer;
        this.studentSpecifics = studentSpecifics;
        this.studentName = studentName;
        this.studentList = studentList;
        this.counter = 1;
        this.check = false;
        this.manager = new ExamManager("bruh", 1, "Exams\\exam1.txt");
    }

    //Getters and Setters
    public boolean getCheck(){
        return this.check;
    }
    public ExamManager getExamManager(){
        return this.manager;
    }
    public String getStudentName(){
        return this.studentName;
    }
    public void setCheck(boolean check){
        this.check = check;
    }
    public void setManager(ExamManager manager){
        this.manager = manager;
    }

    Node hbox;
    Node answer;
    Node betterAnswer;

    @Override
    public void handle(ActionEvent e){

        //This locks the functionality of the Submit button if a test hasn't started
        if(check == true){

            //Checks for the question type of the previously displayed question, gets the answer given and conditionally assigns it a value
            try {
                if(this.manager.getQuestionType(this.counter-1).equals("t")){
                    hbox  = questionAnswer.getChildren().get(0);
                    answer = ((HBox)hbox).getChildren().get(1);
                    int smallCounter = 0;
                    for(int i = 0; i < 2; i++){
                        betterAnswer = ((HBox)answer).getChildren().get(i);
                        if(((RadioButton)betterAnswer).isSelected()){
                            if(i == 0){
                                this.manager.addToAnswers("True");
                            }else if(i == 1){
                                    this.manager.addToAnswers("False");
                                }
                            } else {
                                ++smallCounter;
                            }
                            if(smallCounter == 2){
                                this.manager.addToAnswers("");
                            }
                        }
                }else if(this.manager.getQuestionType(this.counter-1).equals("m")){
                    hbox  = questionAnswer.getChildren().get(0);
                    answer = ((HBox)hbox).getChildren().get(1);
                    int smallCounter = 0;
                    for(int i = 0; i < 4; i++){
                        betterAnswer = ((HBox)answer).getChildren().get(i);
                        if(((RadioButton)betterAnswer).isSelected()){
                            if(i == 0){
                                this.manager.addToAnswers("A");
                                }else if(i == 1){
                                    this.manager.addToAnswers("B");
                                }else if(i == 2){
                                    this.manager.addToAnswers("C");
                                }else if(i == 3){
                                    this.manager.addToAnswers("D");
                                }
                            } else {
                                ++smallCounter;
                            }
                            if(smallCounter == 4){
                                this.manager.addToAnswers("");
                        }
                    }
                }else{
                    hbox = questionAnswer.getChildren().get(0);
                    answer = ((HBox)hbox).getChildren().get(1);
                    this.manager.addToAnswers(((TextField)answer).getText());
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }

            //The HBox containing the displays for the question and answer is then cleared
            questionAnswer.getChildren().clear();
            
            //Gets the next question and answer pair and displays them unless there is no next question
            try {
                if(this.counter != 10){
                    this.boxGen = new HboxGenerator(this.manager.getQuestionType(this.counter), this.manager.getQuestionLineFromFile(this.counter));
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            //Keeps track of the number of times the Submit button has been clicked. When it is not on the last question, 
            //the counter increases. When it is on the last question, the counter resets, the display goes back to its default
            //and we lock the Submit button's functionnality if no tests have been started 
            questionAnswer.getChildren().addAll(this.boxGen.getHBox());
            if(this.counter != 10){
                this.counter++;
            }else{
                this.counter = 1;
                questionAnswer.getChildren().clear();
                questionAnswer.setStyle("-fx-padding: 0 0 10 0;");
                questionAnswer.setSpacing(10);
                TextArea question = new TextArea("");
                question.setWrapText(true);
                question.setPrefWidth(300);
                question.setPrefHeight(40);
                question.setEditable(false);
                TextField answer = new TextField("");
                answer.setPrefWidth(257);
                answer.setStyle("-fx-background-color: grey;");
                questionAnswer.getChildren().addAll(question, answer);
                this.check = false;
                try {

                    //Creates an answer file in UserAnswers
                    Question[] questions = this.manager.getQuestionsArr();
                    Grade grade = new LetterGrade();
                    double percentage = grade.calculatePercentage(grade.extractAnswerPointsAndMaxPoints(questions));
                    String letter = grade.toString();
                    this.manager.createAnswerFile(percentage);

                    //Puts the grade in the correct TextField
                    TextField gradeHolder = (TextField)this.studentSpecifics.getChildren().get(this.manager.getExamNumber());
                    gradeHolder.setText(String.valueOf(letter));

                    //Puts the grade in a Student Array List
                    this.studentList.searchForStudent(this.studentName).addGrade(percentage, this.manager.getExamNumber());

                    //Gets the average and puts it in the correct TextField
                    TextField averageHolder = (TextField)this.studentSpecifics.getChildren().get(4);
                    averageHolder.setText(String.valueOf(this.studentList.searchForStudent(this.studentName).getAverage()));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
