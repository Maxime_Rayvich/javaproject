import java.util.List;
import java.util.ArrayList;
/**
 * holds an array list of students
 * @author Kelsey Pereira Costa
 */
public class Students {
    List<Student> students;
    /**
     * constructor for students
     */
    public Students(){
        this.students = new ArrayList<Student>();
    }
    /**
     * adds a student object to the array list given their name
     * @param name String
     */
    public void addAStudent(String name){
        Student newStudent = new Student(name);
        this.students.add(newStudent);
    }
    /**
     * validates if a student exists in the current list
     * @param name String
     * @return boolean
     */
    public boolean validateIfStudentExists(String name){
        boolean studentChecker = false;
        for (int i = 0; i < this.students.size(); i++){
            if (name.equals(students.get(i).getName())){
                studentChecker = true;
            }
        }
        return studentChecker;
    }
    /**
     * searches for a student with a given name and returns the object
     * @param name String
     * @return Student
     */
    public Student searchForStudent(String name){
        for (int i = 0; i < this.students.size(); i++){
            if (name.equals(students.get(i).getName())){
                return students.get(i);
            }
        }
        throw new IllegalArgumentException("This shouldnt have happened");
    }
    /**
     * returns an index representing where to find a student in the student list
     * @param name String
     * @return int
     */
    public int findStudentIndex(String name){
        int studentIndex = 0;
        for (int i = 0; i < this.students.size(); i++){
            if (name.equals(students.get(i).getName())){
                studentIndex = i;
            }
        }
        return studentIndex;
    }
}
