/**
 * a student object that holds the name and grades of a student
 * @author Kelsey Pereira Costa
 */
public class Student{
    private String name;
    private double[] grade;

    /**
     * constructor for student object
     * @param n name of student
     */
    public Student(String n){
        this.name = n;
        this.grade = new double[]{0,0,0};
    }
    /**
     * get method for name
     * @return name of student
     */
    public String getName(){
        return this.name;
    }
    public double[] getGrade() {
        return this.grade;
    }
    /**
     * overwrites grade of a student given their grade and the test number
     * @param grade double
     * @param testNum int
     */
    public void addGrade(double grade, int testNum){
        this.grade[testNum - 1] = grade;
    }
    /**
     * gets the average of a students grade
     * @return the calculated average (double)
     */
    public double getAverage(){
        double total, sumOfPercentage = 0;
        for(int i = 0; i < this.grade.length; i++){
            sumOfPercentage = this.grade[i] + sumOfPercentage;
        }
        total = sumOfPercentage / this.grade.length;
        return total;
    }
}