/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
public class MultipleChoiceQuestion extends QuestionWithChoice {
    public MultipleChoiceQuestion(String q, String a, String userAnswer){
        if(a.length() > 1 || userAnswer.length() > 1){
            throw(new IllegalArgumentException("Multiple choice answers must be a single letter."));
        }
        this.question = q;
        this.answer = a;
        this.userAnswer = userAnswer;
        this.maxPoints = 1;
        this.gottenPoints = getPoints();
    }

}
