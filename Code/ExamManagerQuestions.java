import java.io.IOException;
import javafx.event.EventHandler;
import javafx.scene.layout.HBox;
import javafx.event.ActionEvent;

public class ExamManagerQuestions implements EventHandler<ActionEvent>{
    private HBox questionAnswer;
    private int number;
    private HboxGenerator boxGen;
    private ExamManager manager;
    private ExamManagerSubmit submit;
    private final int INITIAL_QUESTION = 0;
    public ExamManagerQuestions(HBox questionAnswer, int number, ExamManagerSubmit submit){
        this.questionAnswer = questionAnswer;
        this.number = number;
        this.submit = submit;
    }

    //When the Start Test buttons are clicked, they create the manager associated with the chosen exam,
    //link that manager to ExamManagerSubmit, and displays the first question and answer
    @Override
    public void handle(ActionEvent e){

        //Locks the Start test buttons' functionality if one is already active 
        if(!this.submit.getCheck()){
            this.submit.setCheck(true);
            questionAnswer.getChildren().clear();
            if(this.number == 1){
                this.manager = new ExamManager(submit.getStudentName(), 1, "Exams\\exam1.txt");
            }else if(this.number == 2){
                this.manager = new ExamManager(submit.getStudentName(), 2, "Exams\\exam2.txt");
            }else{
                this.manager = new ExamManager(submit.getStudentName(), 3, "Exams\\exam3.txt");
            }
            submit.setManager(this.manager);
            try {
                this.boxGen = new HboxGenerator(this.manager.getQuestionType(INITIAL_QUESTION), this.manager.getQuestionLineFromFile(INITIAL_QUESTION));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            questionAnswer.getChildren().addAll(this.boxGen.getHBox());
        }   
    }
}
