import java.io.IOException;
import javafx.application.*;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.*;

public class ExamManagerSetup extends Application{
    public ExamManagerSetup(){

    }

    @Override
    public void start(Stage stage){
        //BorderPane container or else the ScrollPane doesn't really work, also it looks good
        BorderPane root = new BorderPane();
        root.setStyle(
        "-fx-base: #373e43;"+
        "-fx-control-inner-background: #93999e;");

        //Scene
        Scene scene = new Scene(root, 700, 300);
        scene.setFill(Color.BLACK);

        //scene is child of stage and BorderPane
        stage.setTitle("ExamManagerSetup");
        stage.setScene(scene);
        stage.show();

        //Building components for the first display
        VBox largestBox = new VBox();
        ScrollPane scroll = new ScrollPane(largestBox);
        root.setCenter(scroll);
        scroll.setFitToHeight(true);
        scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        HBox inputSection = new HBox(); 
        inputSection.setSpacing(10);
        inputSection.setStyle("-fx-padding: 10;");
        Button add = new Button("Add");
        TextField inputStudent = new TextField("Type a new student name here");
        inputStudent.setPrefWidth(180);
        inputStudent.setStyle("-fx-background-color: grey");


        //Component appending
        inputSection.getChildren().addAll(add, inputStudent);
        largestBox.getChildren().addAll(inputSection);
        scroll.setContent(largestBox);
        root.getChildren().addAll(largestBox);
        
        
        //Actions
        ExamManagerAddStudent addStudent = new ExamManagerAddStudent(inputStudent, largestBox);
        add.setOnAction(addStudent);
    }
    public static void main(String[] args) throws IOException{
        //2 Random exams generated here
        RandomExamMaker random = new RandomExamMaker();
        random.generateRandomExam(2);
        random.generateRandomExam(3);
        Application.launch(args);
    }   
}
