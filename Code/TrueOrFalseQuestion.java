/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
public class TrueOrFalseQuestion extends QuestionWithChoice {
    public TrueOrFalseQuestion(String q, String a, String userAnswer){
        if (!a.toLowerCase().equals("true") && !a.toLowerCase().equals("false")){
            throw new IllegalArgumentException("The answer should be 'true' or 'false'.");
        }
        /*note: this part was taken out to enable giving a 'blank' answer when the user submits without entering anything*/
        //if(userAnswer.toLowerCase().charAt(0) != 't' && userAnswer.toLowerCase().charAt(0) != 'f'){
            //throw new IllegalArgumentException("The given answer should begin with the letter 't' or 'f' to be considered.");
        //}
        this.question = q;
        this.answer = a;
        this.userAnswer = userAnswer;
        this.maxPoints = 1;
        this.gottenPoints = getPoints();
    }
}
