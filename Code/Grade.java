public class Grade{

    protected double percentage;
    
    public Grade(){
        this.percentage = -1;
    }
    //This creates an array of doubles that holds the gotten points and the max points of every question in the question array.
    public double[] extractAnswerPointsAndMaxPoints(Question[] question){
        double[] answerArray = new double[question.length*2];
        for(int j = 0; j < answerArray.length; j++){
            answerArray[j+0] = (question[j/2]).getGottenPoints();
            answerArray[j+1] = (question[j/2]).getMaxPoints();
            j++;
             /*else {
                answerArray[j+0] = ((TrueOrFalseQuestion)question[j/2]).getGottenPoints();
                answerArray[j+1] = ((TrueOrFalseQuestion)question[j/2]).getMaxPoints();
                j++;
            }*/
    }
        return answerArray;
    }
    //Calculates the percentage of the whole exam using the previous method.
    public double calculatePercentage(double[] answerArray){
        double pointHolder = 0;
        double maxPointHolder = 0;
        for(int i = 0; i < answerArray.length; i++){
            if(i%2 == 0){
                pointHolder += answerArray[i];
            }else{
                maxPointHolder += answerArray[i];
            }
        }
        this.percentage = (pointHolder/maxPointHolder)*100;
        return this.percentage;
    }

    @Override
    public String toString(){
        return this.percentage+"";
    }
}