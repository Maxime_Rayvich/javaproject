/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
public interface Question{
    double getPoints();
    void readQuestion();
    double getGottenPoints();
    double getMaxPoints();
}