/**
 * @author Maxime Rayvich
 * @version 2021-11-30
 */
public class FillInTheBlank implements Question {
    private String question;
    private String answer;
    private String userAnswer;
    private double maxPoints;
    private double gottenPoints;

    public FillInTheBlank(String q, String a, String userAnswer){
        this.question = q;
        this.answer = a;
        this.userAnswer = userAnswer;
        this.maxPoints = 1;
        this.gottenPoints = getPoints();
    }
    // prints out the question
    @Override
    public void readQuestion(){
        System.out.println(this.question);
    };
    /*points are calculated by comparing the user's answer to the right answer
    In this case, I am only taking account of the first character for the user's answer. This way, if it's 'true', or 'false', 'TRUE', (misspelled) 'TURE', etc it will still work.
    */
    @Override
    public double getPoints(){
        if(this.userAnswer.toLowerCase().equals(this.answer.toLowerCase())){
            return maxPoints;
        }
        else{
            return 0;
        }
    }
    @Override
    public String toString(){
        return 
                  "question is: " + this.question 
                + " correct answer is: " + this.answer 
                + " given answer is: " + this.userAnswer 
                + " the max points for this question is: " + this.maxPoints 
                + " the points obtained are: " + this.gottenPoints;
    }
    public String getUserAnswer(){
        return this.userAnswer;
    }
    public double getMaxPoints(){
        return this.maxPoints;
    }
    public double getGottenPoints(){
        return this.gottenPoints;
    }
    
}


    

