import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextArea;
import javafx.event.ActionEvent;

public class ExamManagerAddStudent implements EventHandler<ActionEvent>{
    private VBox largestBox;
    private TextField inputString;
    private Students studentList;
    public ExamManagerAddStudent(TextField inputString, VBox vBox){
        this.inputString = inputString;
        this.largestBox = vBox;
        this.studentList = new Students();
    }

    @Override
    public void handle(ActionEvent e){

        //Managing the this.studentList ArrayList
        if(!this.studentList.validateIfStudentExists(this.inputString.getText())){
            this.studentList.addAStudent(this.inputString.getText());

            //Creating the GUI for a student
            VBox student = new VBox();
            student.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;"
            + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
            + "-fx-border-radius: 5;" + "-fx-border-color: #d1d8de;");

            //Section for the read-only title displays and the test buttons
            HBox buttons = new HBox();
            buttons.setStyle("-fx-padding: 0 0 10 0;");
            buttons.setSpacing(10);
            TextField studentNameRead = new TextField("Student Name");
            studentNameRead.setPrefWidth(150);
            studentNameRead.setEditable(false);
            Button test1 = new Button("Start test 1");
            test1.setPrefWidth(75);
            Button test2 = new Button("Start test 2");
            test2.setPrefWidth(75);
            Button test3 = new Button("Start test 3");
            test3.setPrefWidth(75);
            TextField testAverageRead = new TextField("Average");
            testAverageRead.setEditable(false);
            
            //Section for the student name and their grades display
            HBox studentSpecifics = new HBox();
            studentSpecifics.setStyle("-fx-padding: 0 0 10 0;");
            studentSpecifics.setSpacing(10);
            TextField studentName = new TextField(this.inputString.getText());
            studentName.setPrefWidth(150);
            studentName.setEditable(false);
            TextField test1Grade = new TextField("");
            test1Grade.setPrefWidth(75);
            test1Grade.setEditable(false);
            test1Grade.setText("0.0");
            TextField test2Grade = new TextField("");
            test2Grade.setPrefWidth(75);
            test2Grade.setEditable(false);
            test2Grade.setText("0.0");
            TextField test3Grade = new TextField("");
            test3Grade.setPrefWidth(75);
            test3Grade.setEditable(false);
            test3Grade.setText("0.0");
            TextField testAverage = new TextField("");
            testAverage.setEditable(false);
            testAverage.setText("0.0");
            
            //Section for the default question display and answer display
            HBox questionAnswer = new HBox();
            questionAnswer.setStyle("-fx-padding: 0 0 10 0;");
            questionAnswer.setSpacing(10);
            TextArea question = new TextArea("");
            question.setWrapText(true);
            question.setPrefWidth(300);
            question.setPrefHeight(40);
            question.setEditable(false);
            TextField answer = new TextField("");
            answer.setPrefWidth(257);
            answer.setStyle("-fx-background-color: grey;");
            
            //Submit button
            Button submit = new Button("Submit");

            //Component appending
            questionAnswer.getChildren().addAll(question, answer);
            studentSpecifics.getChildren().addAll(studentName, test1Grade, test2Grade, test3Grade, testAverage);
            buttons.getChildren().addAll(studentNameRead, test1, test2, test3, testAverageRead);
            student.getChildren().addAll(buttons, studentSpecifics, questionAnswer, submit);
            this.largestBox.getChildren().add(student);

            //Actions
            ExamManagerSubmit submitAction = new ExamManagerSubmit(questionAnswer, studentSpecifics, this.inputString.getText(), this.studentList);
            submit.setOnAction(submitAction);
            ExamManagerQuestions startTest1 = new ExamManagerQuestions(questionAnswer, 1, submitAction);
            test1.setOnAction(startTest1);
            ExamManagerQuestions startTest2 = new ExamManagerQuestions(questionAnswer, 2, submitAction);
            test2.setOnAction(startTest2);
            ExamManagerQuestions startTest3 = new ExamManagerQuestions(questionAnswer, 3, submitAction);
            test3.setOnAction(startTest3);
            
        }else{
            Node largest = largestBox.getChildren().get(0);
            TextField inputStudent = (TextField)((HBox)largest).getChildren().get(1);
            inputStudent.setText("Student already exists!");
        }
        
    }
    
}
