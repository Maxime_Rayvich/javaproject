import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestGrade {

    //Test for the extraction of a grade and calculate percentage
    @Test
    public void testExtract(){
        Question[] questions = new Question[1];
        questions[0] = new MultipleChoiceQuestion("","s","a");
        Grade pGrade = new Grade();
        assertEquals(0, pGrade.calculatePercentage(pGrade.extractAnswerPointsAndMaxPoints(questions)));
    }

    //Tests for the functionality of LetterGrade
    @Test
    public void testToLetter(){
        Question[] questions = new Question[2];
        questions[0] = new TrueOrFalseQuestion("","true","true");
        questions[1] = new TrueOrFalseQuestion("","true","false");
        Grade lGrade = new LetterGrade();
        double percentage = lGrade.calculatePercentage(lGrade.extractAnswerPointsAndMaxPoints(questions));
        assertEquals(50, percentage);
        assertEquals("D", ((LetterGrade)lGrade).toLetter());
        }
}
