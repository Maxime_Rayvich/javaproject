import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingExamManager {
    @Test
    public void testConstructorWithTwoInvalidInput(){
        try{
            ExamManager em = new ExamManager("User_Name", -1, "..\\path_that_doesn't_exist:PPPP");
            fail("should have caught an exception but didn't");
        }
        catch(IllegalArgumentException e){
            //did what it was supposed to
        }
        catch(Exception e){
            fail("threw exception of the wrong type");
        }
    }
    @Test
    public void testConstructorWithFirstInvalidInput(){
        try{
            ExamManager em = new ExamManager("User_Name", -1, "Exams\\Exam2.txt");
            fail("should have caught an exception but didn't");
        }
        catch(IllegalArgumentException e){
            //did what it was supposed to
        }
        catch(Exception e){
            fail("threw exception of the wrong type");
        }
    }
    @Test
    public void testConstructorWithSecondInvalidInput(){
        try{
            ExamManager em = new ExamManager("User_Name", 1, "doesn'texist");
            fail("should have caught an exception but didn't");
        }
        catch(IllegalArgumentException e){
            //did what it was supposed to
        }
        catch(Exception e){
            fail("threw exception of the wrong type");
        }
    }
    @Test 
    public void testConstructorValid(){
            ExamManager em = new ExamManager("User_Name", 1, "I:\\Java 3\\JAVA PROJECT\\javaproject\\Exams\\Exam2.txt");
    }
    
 
    @Test 
    public void testCreateAnswerFile() throws IOException{
        ExamManager em = new ExamManager("aVeryUniqueUsername", 1, "I:\\Java 3\\JAVA PROJECT\\javaproject\\Exams\\Exam2.txt");
        //we are trying to run it before having run promptUserToAnswer, which should throw an exception.
        try{
            em.createAnswerFile(1);
            fail("Should have caught an exception");
        }
        catch(UnsupportedOperationException e){
            System.out.println("Caught it!!");
        }
        catch(Exception e){
            fail("caught the wrong exception");
        }
    }
    @Test
    public void testExtract_file() throws IOException{
        // really, by knowing that the prompt method is working we kinda know this one is too.
       
        //we'll compare what we get from the method to a hard coded List<String>
        //first a correct one
        ExamManager em = new ExamManager("aVeryUniqueUsername", 1, "D:\\Java 3\\JAVA PROJECT\\javaproject\\Exams\\Exam2.txt");
        List<String> data = em.extract_file(em.getExamURL());
        List<String> dataToCompare = new ArrayList<>();
        dataToCompare.add("t;Anakin doesn't like sand;true");
        dataToCompare.add("m;anakin:a) likes sand, b) doesn't like sand, c) murders not the men but the women too, d) aaaaaaaaaa;a");
        dataToCompare.add("t;drink;false");
        dataToCompare.add("f; fill in the blank _ is the god of all the word of humanity;HaLlAm");
        assertEquals(data, dataToCompare);

        //now an incorrect one.
        List<String> badDataToCompare = new ArrayList<>();
        badDataToCompare.add("t;Anakin doesn't like sand;true");
        badDataToCompare.add("m;asdfasdkfasdfjaskdfkjasdkfders not the men but the women too, d) aaaaaaaaaa;a");
        badDataToCompare.add("t;drink;false");
        badDataToCompare.add("f; fill in the blank _ is the god of all the word of humanity;HaLlAm");
        assertNotEquals(data, badDataToCompare);

    





    }
    @Test
    public void testGetQuestionsArr(){
        //first let's check that this catches when answers has not been initialized.
        ExamManager em = new ExamManager("aVeryUniqueUsername", 1, "D:\\Java 3\\JAVA PROJECT\\javaproject\\Exams\\Exam2.txt");
        //we have not assigned examManager.
        try{
            em.getQuestionsArr();
            fail("Should have thrown an exception");
        }
        catch(NullPointerException e){
            System.out.println("caught it");
        }
        catch(Exception e){
            System.out.println("caught wrong exception");
        }
        //further testing in other class, since user input...

    }
}
