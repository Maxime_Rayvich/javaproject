import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingTrueOrFalseQuestion {
    @Test
    public void TestConstructorWithTwoInvalidInputs(){
        try{
            TrueOrFalseQuestion tf = new TrueOrFalseQuestion("question", "tRuf", "rr");
            fail("should have caught an exception but didn't");
        }
            catch(IllegalArgumentException e){
                //did what it was supposed to
            }
            catch(Exception e){
                fail("threw exception of the wrong type.");
            }
        }
        @Test
        public void TestConstructorWithTwoInvalidInputsCapital(){
            try{
                TrueOrFalseQuestion tf = new TrueOrFalseQuestion("question", "TRUF", "RR");
                fail("should have caught an exception but didn't");
            }
                catch(IllegalArgumentException e){
                    //did what it was supposed to
                }
                catch(Exception e){
                    fail("threw exception of the wrong type.");
                }
            }
        @Test
        public void TestConstructorWithFirstInvalidInputs(){
            try{
                TrueOrFalseQuestion tf = new TrueOrFalseQuestion("question", "truf", "false");
                fail("should have caught an exception but didn't");
            }
                catch(IllegalArgumentException e){
                    //did what it was supposed to
                }
                catch(Exception e){
                    fail("threw exception of the wrong type.");
                }
            }
                @Test
                public void TestConstructorValidInput(){
                    try{
                        TrueOrFalseQuestion mult = new TrueOrFalseQuestion("question", "FALSE", "FAAA");
                    }
                    catch(Exception e){
                        fail("Threw exception " + e +" when it shouldn't have");
                    }
                    try{
                        TrueOrFalseQuestion mult2 = new TrueOrFalseQuestion("question", "TRUE", "truee though for real");
                    }
                    catch(Exception e){
                        fail("Threw exception " + e +" when it shouldn't have");
                    }
                    try{
                        TrueOrFalseQuestion mult3 = new TrueOrFalseQuestion("question", "false", "trueeeeeeeeeee");
                    }
                    catch(Exception e){
                        fail("Threw exception " + e +" when it shouldn't have");
                    }
                    try{
                        TrueOrFalseQuestion mult4 = new TrueOrFalseQuestion("question", "true", "TrAmPoLiNe (I'm abusing the system!!)");
                    }
                    catch(Exception e){
                        fail("Threw exception " + e +" when it shouldn't have");
                    }
                }
                @Test
                public void testGetPoints(){
                    //should be case insensitive when correct:
                    TrueOrFalseQuestion mult = new TrueOrFalseQuestion("the sky is blue", "false", "false");
                    double t = mult.getPoints();
                    assertEquals(1, t);
                    TrueOrFalseQuestion mult2 = new TrueOrFalseQuestion("the sky is blue", "TRUE", "true");
                    double f = mult2.getPoints();
                    assertEquals(1, f);
                    TrueOrFalseQuestion mult3 = new TrueOrFalseQuestion("the sky is blue", "false", "FALSE");
                    double k = mult3.getPoints();
                    assertEquals(1, k);
                    TrueOrFalseQuestion mult4 = new TrueOrFalseQuestion("the sky is blue", "TRUE", "TRUE");
                    double l = mult4.getPoints();
                    assertEquals(1, l);
                }

                @Test
                public void testGetPointsIncorrect(){
                    //should be case insensitive when incorrect:
                    TrueOrFalseQuestion fault = new TrueOrFalseQuestion("the sky is blue", "true", "false");
                    double f1 = fault.getPoints();
                    assertEquals(0, f1);
                    TrueOrFalseQuestion fault2 = new TrueOrFalseQuestion("the sky is blue", "FALSE", "true");
                    double f2 = fault2.getPoints();
                    assertEquals(0, f2);
                    TrueOrFalseQuestion fault3 = new TrueOrFalseQuestion("the sky is blue", "false", "true");
                    double f3 = fault3.getPoints();
                    assertEquals(0, f3);
                    TrueOrFalseQuestion fault4 = new TrueOrFalseQuestion("the sky is blue", "TRUE", "FALSE");
                    double f4 = fault4.getPoints();
                    assertEquals(0, f4);
                    }
                
            }


    

